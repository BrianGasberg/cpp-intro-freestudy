// Compile from command-line with:
//
// GCC/G++:
//
//    g++ -std=c++11 helloworld.cpp
//
// Clang:n
//
//    clang++ -std=c++11 helloworld.cpp

#include <iostream>

int main()
{
    // This is a totally unnecessary C++11 lambda function
    auto say_hello = [](){ std::cout << "hello c++11" << std::endl; };

    // Lets invoke it.
    say_hello();

    return 0;
}
