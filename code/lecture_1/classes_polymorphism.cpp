#include <iostream>
#include <string>

class lecture
{
public: // Available to anybody

    // Return true if passed exam
    virtual bool take_exam() = 0;
};

class math_lecture : public lecture
{
public:

    virtual bool take_exam()
    {
        std::cout << "What is 2 + 2" << std::endl;
        int answer;
        std::cin >> answer;

        return answer == 4;
    }
};

class history_lecture : public lecture
{
public:

    virtual bool take_exam()
    {
        std::cout << "When was the first C++ standard released" << std::endl;
        int answer;
        std::cin >> answer;

        return answer == 1998;
    }
};


int main()
{
    lecture* math = new math_lecture();
    std::cout << math->take_exam() << std::endl;

    lecture* history = new history_lecture();
    std::cout << history->take_exam() << std::endl;

    return 0;
}
