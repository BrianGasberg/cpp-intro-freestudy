#include <iostream>
#include <string>

class lecture
{
public: // Available to anybody

    lecture(const std::string& course)
        : m_course(course)
    { }

    ~lecture()
    {
        // Cleanup code
    }

    std::string date()
    {
        return "25 feb. 2015";
    }

private:

    std::string m_course;

};

int main()
{
    lecture lec1("cpp");
    std::cout << lec1.date() << std::endl;

    return 0;
}
